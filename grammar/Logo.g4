grammar Logo ; 

@header {		
  package logoparsing;
}

FLOAT : [0-9][0-9]*('.'[0-9]+)? ;
WS : [ \t\r\n]+ -> skip ;
COMMENT1 : '//' .*? [\r\n]+ -> skip;
COMMENT2 : '/*' .*? '*/' -> skip;
SYMBOL : [a-zA-Z][a-zA-Z]*;

programme :
 liste_instructions  
;

liste_instructions :   
 (instruction)+    
;

instruction :
   'av' expr # av
 | 'td' expr # td
 | 'tg' expr # tg
 | 'lc' expr # lc
 | 'bc' expr # bc
 | 'fcap' expr # fcap
 | 're' expr # re
 | 'donne "' SYMBOL expr # affectation
 | 'fcc' expr # change_color
 | 'move' # move
 | 'store' # store
 | 'repete' expr '[' liste_instructions ']' # loop
 | 'tantque' booleen '[' liste_instructions ']' #while
 | 'si' booleen '['  liste_instructions']' #if
 | 'si' booleen '[' liste_instructions']' '[' liste_instructions']' #ifElse
;

expr :
    expr ('*' | '/' ) expr # mult
    | expr ('+' | '-' ) expr # sum
    | FLOAT         # float
    | '(' expr ')'  # parenthese
    | ':' SYMBOL # identification
    | 'hasard(' expr ')' # random
    | 'cos(' expr ')' # cos
    | 'sin(' expr ')' # sin
    | 'loop' # loopVariable
;

booleen :
    expr '>' expr # greater
    | expr '<' expr # lower
    | expr '==' expr # equal
    | booleen '||' booleen # or
    | booleen '&&' booleen # and
    | expr '!=' expr # different
;