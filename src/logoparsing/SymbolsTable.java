package logoparsing;

import java.util.ArrayDeque;
import java.util.Deque;

public class SymbolsTable {
    private Deque<Variable> storedSymbols = new ArrayDeque<>();

    public void addVariable(Variable variable) {
        storedSymbols.push(variable);
    }


    public Variable getVariableByName(String variableName) {
        for (Variable variable: storedSymbols) {
            if (variable.getName().equals(variableName)) {
                return variable;
            }
        }
        return null;
    }

    public void modifyValue(String variableName, Double variableValue) {
        getVariableByName(variableName).setValue(variableValue);
    }

    public void deleteVariablesUntilFlag() {
        Variable variable;
        do {
            variable = storedSymbols.pop();
        } while ( ! variable.isFlag());
    }

    public void addFlag() {
        addVariable(new Variable(true));
    }
}
