package logoparsing;

public class Variable {

    private Double value;
    private String name;
    private boolean isFlag;

    public Variable(double value, String name) {
        this.value = value;
        this.name = name;
        this.isFlag = false;
    }

    public Variable(boolean isFlag) {
        this.isFlag = isFlag;
        this.name = "";
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public boolean isFlag() {
        return isFlag;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
