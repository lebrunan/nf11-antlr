/*
  * Created on 12 may. 2018
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package logoparsing;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;
import logogui.GraphParameter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Traceur {
	private Color couleur = Color.BLACK;
	List<Color> colorList = Arrays.asList(
			Color.BLACK,
			Color.ALICEBLUE,
			Color.AZURE,
			Color.DARKVIOLET,
			Color.LIME,
			Color.RED,
			Color.FIREBRICK,
			Color.GREENYELLOW,
			Color.FUCHSIA);
	private double initx = 350, inity = 350; // position initiale
	private double posx = initx, posy = inity; // position courante
	private double angle = 90;
	private double teta;
	private boolean isTracing = true;
	ObjectProperty<GraphParameter> line;

	public Traceur() {
		setTeta();
		line = new SimpleObjectProperty<GraphParameter>();
	}

	ObjectProperty<GraphParameter> lineProperty() {
		return line;
	}

	private void setTeta() {
		teta = Math.toRadians(angle);
	}

	private void addLine(double x1, double y1, double x2, double y2) {
		if (isTracing) {
			line.setValue(new GraphParameter(x1, y1, x2, y2, couleur));
		}
	}

	public void avance(double r) {
		double a = posx + r * Math.cos(teta);
		double b = posy - r * Math.sin(teta);
		addLine(posx, posy, a, b);

		posx = a;
		posy = b;
	}

	public void recule(Double r) {
		double a = posx - r * Math.cos(teta);
		double b = posy + r * Math.sin(teta);
		addLine(posx, posy, a, b);

		posx = a;
		posy = b;
	}

	public void td(double r) {
		angle = (angle - r) % 360;
		setTeta();
	}

	public void tg(double r) {
		angle = (angle + r) % 360;
		setTeta();
	}

	public void fcap(double r) {
		angle = r;
		setTeta();
	}

	public Turtle getPosition() {
		return new Turtle(
				posx, posy, angle
		);
	}

	public void moveTo(Turtle position) {
		this.posx = position.getPosition_x();
		this.posy = position.getPosition_y();
		this.angle = position.getAngle();
	}

	public void changeColor(Double colorChosen) {
		this.couleur = colorList.get((int) (colorChosen % colorList.size()));
	}

	public void lc() {
		this.isTracing = false;
	}

	public void bc() {
		this.isTracing = true;
	}

	public void re(Double r) {
	}
}
