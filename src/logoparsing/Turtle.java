package logoparsing;

public class Turtle {
    private double position_x;
    private double position_y;
    private double angle;

    public Turtle(double position_x, double position_y, double angle) {
        this.position_x = position_x;
        this.position_y = position_y;
        this.angle = angle;
    }

    public double getPosition_x() {
        return position_x;
    }

    public double getPosition_y() {
        return position_y;
    }

    public double getAngle() {
        return angle;
    }
}
