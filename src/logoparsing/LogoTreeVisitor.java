package logoparsing;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import logoparsing.LogoParser.AvContext;
import logoparsing.LogoParser.FloatContext;
import logoparsing.LogoParser.ParentheseContext;
import logoparsing.LogoParser.TdContext;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Random;
import java.util.stream.Collectors;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class LogoTreeVisitor extends LogoBaseVisitor<Integer> {
	Traceur traceur;
	StringProperty log = new SimpleStringProperty();
	private Double loopCurrentValue;
	private Deque<Turtle> storedPositions = new ArrayDeque<>();
	private SymbolsTable symbolsTable = new SymbolsTable();
	public LogoTreeVisitor() {
		traceur = new Traceur();
	}

	public StringProperty logProperty() {
		return log;
	}

	public Traceur getTraceur() {
		return traceur;
	}

	/*
	 * Map des attributs associés à chaque noeud de l'arbre
	 * key = node, value = valeur de l'expression du node
	 */
	ParseTreeProperty<Double> atts = new ParseTreeProperty<Double>();

	public void setValue(ParseTree node, double value) {
		atts.put(node, value);
	}

	public double getValue(ParseTree node) {
		Double value = atts.get(node);
		if (value == null) {
			throw new NullPointerException();
		}
		return value;
	}

// Instructions de base	


	@Override
	public Integer visitAffectation(LogoParser.AffectationContext ctx) {
		String variableName;
		Binome<Double> variableValue;
			variableName = ctx.SYMBOL().getText();
			variableValue = evaluate(ctx.expr());
			if (symbolsTable.getVariableByName(variableName) != null) {
				symbolsTable.modifyValue(variableName,variableValue._2);
			} else {
				symbolsTable.addVariable(new Variable(variableValue._2, variableName));
			}
		return variableValue._1;
	}

	@Override
	public Integer visitIdentification(LogoParser.IdentificationContext ctx) {
		String variableName = ctx.SYMBOL().getText();
		Variable variable = symbolsTable.getVariableByName(variableName);
		if (variable != null) {
			setValue(ctx,variable.getValue());
			return 0;
		} else {
			System.out.println(variableName + " n'a pas été déclarée");
			return -1;
		}
	}

	@Override
	public Integer visitWhile(LogoParser.WhileContext ctx) {

		symbolsTable.addFlag();

		Binome<Double> condition = evaluate(ctx.booleen());
		while (condition._2 == 1 && condition._1 == 0) {
			System.out.println("ahhh");
			visit(ctx.liste_instructions());
			condition = evaluate(ctx.booleen());
		}

		symbolsTable.deleteVariablesUntilFlag();

		return condition._1;
	}

	@Override
	public Integer visitGreater(LogoParser.GreaterContext ctx) {
		Binome<Double> left, right;
		left = evaluate(ctx.expr(0));
		right = evaluate(ctx.expr(1));
		if (left._1 != 0 || right._1 != 0) {
			return -1;
		}
		setValue(ctx, left._2 > right._2 ? 1 : 0);
		return 0;
	}

	@Override
	public Integer visitLower(LogoParser.LowerContext ctx) {

		Binome<Double> left, right;
		left = evaluate(ctx.expr(0));
		right = evaluate(ctx.expr(1));
		if (left._1 != 0 || right._1 != 0) {
			return -1;
		}
		setValue(ctx, left._2 < right._2 ? 1 : 0);
		return 0;
	}

	@Override
	public Integer visitEqual(LogoParser.EqualContext ctx) {
		Binome<Double> left, right;
		left = evaluate(ctx.expr(0));
		right = evaluate(ctx.expr(1));
		if (left._1 != 0 || right._1 != 0) {
			return -1;
		}
		setValue(ctx, left._2.equals(right._2) ? 1 : 0);
		return 0;
	}


	@Override
	public Integer visitDifferent(LogoParser.DifferentContext ctx) {
		Binome<Double> left, right;
		left = evaluate(ctx.expr(0));
		right = evaluate(ctx.expr(1));
		if (left._1 != 0 || right._1 != 0) {
			return -1;
		}
		setValue(ctx, ! left._2.equals(right._2) ? 1 : 0);
		return 0;
	}

	@Override
	public Integer visitOr(LogoParser.OrContext ctx) {
		Binome<Double> left, right;
		left = evaluate(ctx.booleen(0));
		right = evaluate(ctx.booleen(1));
		if (left._1 != 0 || right._1 != 0) {
			return -1;
		}
		setValue(ctx, left._2 == 1 || right._2 == 1 ? 1 : 0);
		return 0;
	}

	@Override
	public Integer visitAnd(LogoParser.AndContext ctx) {
		Binome<Double> left, right;
		left = evaluate(ctx.booleen(0));
		right = evaluate(ctx.booleen(1));
		if (left._1 != 0 || right._1 != 0) {
			return -1;
		}
		setValue(ctx, left._2 == 1 && right._2 == 1 ? 1 : 0);
		return 0;
	}

	@Override
	public Integer visitIf(LogoParser.IfContext ctx) {

		symbolsTable.addFlag();

		Binome<Double> condition = evaluate(ctx.booleen());
		// Expression is correct
		if (condition._1 == 0) {
			// if condition is valid, visit instruction. If not, ignore.
			if (condition._2 == 1) {
				visit(ctx.instruction());
			}
		}
		symbolsTable.deleteVariablesUntilFlag();

		return -1;

	}

	@Override
	public Integer visitIfElse(LogoParser.IfElseContext ctx) {

		symbolsTable.addFlag();

		Binome<Double> condition = evaluate(ctx.booleen());
		// Expression is correct
		if (condition._1 == 0) {
			// if condition is valid, visit first instruction. If not, visit second one
			if (condition._2 == 1) {
				visit(ctx.instruction(0));
			} else {
				visit(ctx.instruction(1));
			}
		}
		symbolsTable.deleteVariablesUntilFlag();

		return condition._1;
	}

	@Override
	public Integer visitTd(TdContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.td(bilan._2);
		}
		return 0;
	}

	@Override
	public Integer visitAv(AvContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.avance(bilan._2);
			log.setValue("Avance de  " + bilan._2);
			log.setValue("\n");
		}
		return bilan._1;
	}

	@Override
	public Integer visitTg(LogoParser.TgContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.tg(bilan._2);
		}
		return 0;
	}

	@Override
	public Integer visitLc(LogoParser.LcContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.lc();
		}
		return 0;
	}

	@Override
	public Integer visitBc(LogoParser.BcContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.bc();
		}
		return 0;
	}

	@Override
	public Integer visitRe(LogoParser.ReContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.recule(bilan._2);
		}
		return 0;	}

	@Override
	public Integer visitFcap(LogoParser.FcapContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.fcap(bilan._2);
		}
		return 0;
	}

	// Expressions

	@Override
	public Integer visitParenthese(ParentheseContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			setValue(ctx, bilan._2);
		}
		return bilan._1;
	}

	@Override
	public Integer visitLoop(LogoParser.LoopContext ctx) {

		//Flag la table des symboles que le programme est dans un sous-bloc d'instruction.
		symbolsTable.addFlag();

		Binome<Double> loopVar;
		loopVar = evaluate(ctx.expr());
		for (double i = 1; i <= loopVar._2; i++) {
			this.loopCurrentValue = i;
			visit(ctx.liste_instructions());
			this.loopCurrentValue = null;
		}

		symbolsTable.deleteVariablesUntilFlag();
		return loopVar._1;
	}

	@Override
	public Integer visitLoopVariable(LogoParser.LoopVariableContext ctx) {
		setValue(ctx, this.loopCurrentValue);
		return 0;
	}

	@Override
	public Integer visitFloat(FloatContext ctx) {
		String floatText = ctx.FLOAT().getText();
		setValue(ctx, Double.valueOf(floatText));
		return 0;
	}

	@Override
	public Integer visitStore(LogoParser.StoreContext ctx) {
		this.storedPositions.push(this.traceur.getPosition());
		return 0;
	}

	@Override
	public Integer visitMove(LogoParser.MoveContext ctx) {
		traceur.moveTo(this.storedPositions.pop());
		return 0;
	}

	@Override
	public Integer visitRandom(LogoParser.RandomContext ctx) {
		Integer result = 0;
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {

			Random rand = new Random();
			setValue(ctx, rand.nextInt((int) (bilan._2 + 1)));

		} else {
			result = bilan._1;
		}
		return result;
	}

	@Override
	public Integer visitCos(LogoParser.CosContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {

			setValue(ctx, cos(Math.toRadians(bilan._2)));

			return 0;
		}
		return bilan._1;
	}

	@Override
	public Integer visitChange_color(LogoParser.Change_colorContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {
			traceur.changeColor(bilan._2);
		}
		return bilan._1;
	}

	@Override
	public Integer visitSin(LogoParser.SinContext ctx) {
		Binome<Double> bilan = evaluate(ctx.expr());
		if (bilan._1 == 0) {

			setValue(ctx, sin(Math.toRadians(bilan._2)));

			return 0;
		}
		return bilan._1;
	}

	@Override
	public Integer visitSum(LogoParser.SumContext ctx) {
		Binome<Double> left, right;
		try {
			left = evaluate(ctx.expr(0));
			right = evaluate(ctx.expr(1));
			if (left._1 == 0 && right._1 == 0) {
				double r = ctx.getChild(1).getText().equals("+") ?
						left._2 + right._2 : left._2 - right._2;
				setValue(ctx, r);
			} else
				return left._1 == 0 ? right._1 : left._1;
		} catch (NullPointerException ex) {
			ex.printStackTrace(); }
		return 0;
	}


	@Override
	public Integer visitMult(LogoParser.MultContext ctx) {
		Binome<Double> left, right;
		try {
			left = evaluate(ctx.expr(0));
			right = evaluate(ctx.expr(1));
			if (left._1 == 0 && right._1 == 0) {
				if (right._2 == 0 && ctx.getChild(1).getText().equals("*")) {
					setValue(ctx, 1);
					return -1;
				}
				double r = ctx.getChild(1).getText().equals("*") ?
						left._2 * right._2 : left._2 / right._2;
				setValue(ctx, r);
			} else
				return left._1 == 0 ? right._1 : left._1;
		} catch (NullPointerException ex) {
			ex.printStackTrace(); }
		return 0;
	}

	private Binome<Double> evaluate(ParseTree expr) {
		Binome<Double> res = new Binome<>();
		res._1 = visit(expr);
		res._2 = res.isValid() ? getValue(expr) : Double.POSITIVE_INFINITY;
		return res;
	}

	private class Binome<T> {
		public Integer _1; // Validity of expression
		public T _2; // Value of expression

		public boolean isValid() {
			return _1 == 0;
		}
	}
}
